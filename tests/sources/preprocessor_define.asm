;; This listing serves as a test for the @define preprocessor directive

@define dest 0x1234
@define cool.Instr NOP
@define ßasd 0x42
@define smem 0x524

mov r4, dest
cool.Instr
jmpr cc_eq, 0x00
