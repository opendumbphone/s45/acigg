; This listing serves as a test for the linker

@org 0x101234

entry:
  nop
  cmp r1, r1
  jmpr cc_eq, .exit
  jnb ~MDH.14, entry
  jmpr cc_uc, entry
  jmpa cc_slt, entry
  jmps entry
@org 0x101288
  .exit:
    nop

