;; This listing tests different register encodings

@regset s45

add ~MDH, 0xF0F1 ; reg, immediate
add r12, [&MDH] ; SFR as memory address
add [&MDH], r12 ; same
add ~MDH, [&MDL] ; one as reg, other as memory
BMOV ~MDH.1, ~MDL.7 ; with bitoffsets (reg decoding)
