;; This listing includes an example for all supported instructions, using all documented opcodes
;; TODO: Implement a reference binary

global label tests {
  add r12, r13 
  add r12, [r1]
  add r12, [r1+]
  add r12, 7
  add r12, 0xBEEF
  add %S0TBUF, 0xBEEF
  add r12, [0xFF12]
  add [0xFF12], r13

  addc r12, r13 
  addc r12, [r1]
  addc r12, [r1+]
  addc r12, 7
  addc r12, 0xBEEF
  addc %S0TBUF, 0xBEEF
  addc r12, [0xFF12]
  addc [0xFF12], r13

  and r12, r13 
  and r12, [r1]
  and r12, [r1+]
  and r12, 7
  and r12, 0xBEEF
  and %S0TBUF, 0xBEEF
  and r12, [0xFF12]
  and [0xFF12], r13

  or r12, r13 
  or r12, [r1]
  or r12, [r1+]
  or r12, 7
  or r12, 0xBEEF
  or %S0TBUF, 0xBEEF
  or r12, [0xFF12]
  or [0xFF12], r13

  sub r12, r13 
  sub r12, [r1]
  sub r12, [r1+]
  sub r12, 7
  sub r12, 0xBEEF
  sub %S0TBUF, 0xBEEF
  sub r12, [0xFF12]
  sub [0xFF12], r13

  subc r12, r13 
  subc r12, [r1]
  subc r12, [r1+]
  subc r12, 7
  subc r12, 0xBEEF
  subc %S0TBUF, 0xBEEF
  subc r12, [0xFF12]
  subc [0xFF12], r13

  xor r12, r13 
  xor r12, [r1]
  xor r12, [r1+]
  xor r12, 7
  xor r12, 0xBEEF
  xor %S0TBUF, 0xBEEF
  xor r12, [0xFF12]
  xor [0xFF12], r13

  ashr r12, r13
  ashr r12, 0xF

  rol r12, r13
  rol r12, 0xF

  ror r12, r13
  ror r12, 0xF

  shl r12, r13
  shl r12, 0xF

  shr r12, r13
  shr r12, 0xF

  band r5.3, r4.2
  bcmp r5.3, r4.2
  bmov r5.3, r4.2
  bmovn r5.3, r4.2
  bor r5.3, r4.2
  bxor r5.3, r4.2

  bclr r4.2
  bset r4.2

  bfldh r4, 0b01010101, 0xCD
  bfldl r4, 0b01010101, 0xCD

  exts r12, 4
  exts 0xFA, 4

  extsr r12, 4
  extsr 0xFA, 4

  nop
  reti
  rets
  srst
  idle
  pwrdn
  srvwdt
  diswdt
  einit

  cpl r12
  neg r12

  div r12
  divl r12
  divlu r12
  divu r12

  mul r12, r13
  mulu r12, r13

  cmpd1 r12, 0xF
  cmpd1 r12, 0xBEEF
  cmpd1 r12, [0xFF12]

  cmpd2 r12, 0xF
  cmpd2 r12, 0xBEEF
  cmpd2 r12, [0xFF12]

  cmpi1 r12, 0xF
  cmpi1 r12, 0xBEEF
  cmpi1 r12, [0xFF12]

  cmpi2 r12, 0xF
  cmpi2 r12, 0xBEEF
  cmpi2 r12, [0xFF12]

  cmp r12, r13 
  cmp r12, [r1]
  cmp r12, [r1+]
  cmp r12, 7
  cmp r12, 0xBEEF
  cmp %S0TBUF, 0xBEEF

  calla cc_UC, 0xABCE
  jmpa cc_UC, 0xABCE

  calli cc_UC, [r12]
  jmpi cc_UC, [r12]

  extp r12, 4
  extp 0x3ff, 4

  extpr r3, 4
  extpr 0x3ff, 4

  mov r12, r13
  mov r12, 0xF
  mov r12, 0xBEEF
  mov r12, [r13]
  mov r12, [r13+]
  mov [r12], r13
  mov [-r12], r13
  mov [r12], [r13]
  mov [r12+], [r13]
  mov [r12], [r13+]
  mov r12, [r13 + 0xBEEF]
  mov [r12 + 0xBEEF], r13
  mov [r12], [0xFF12]
  mov [0xFF12], [r12]
  mov %S0TBUF, [0xFF12]
  mov [0xFF12], %S0TBUF

  movbs r12, rl4
  movbs %S0TBUF, [0xFF12]
  movbs [0xFF12], %S0TBUF

  movbz r12, rl4
  movbz %S0TBUF, [0xFF12]
  movbz [0xFF12], %S0TBUF

  calls 0xFA, 0xABCD
  jmps 0xFA, 0xABCD

  callr 0x00
  jmpr cc_UC, 0x00

  jb r4.2, 0x00
  jbc r4.2, 0x00
  jnb r4.2, 0x00
  jnbs r4.2, 0x00

  pcall %S0TBUF, 0x1234

  pop %S0TBUF
  push %S0TBUF
  retp %S0TBUF

  scxt r12, 0xBEEF
  scxt %S0TBUF, [0xFF12]

  prior r12, r13

  trap 0x7F

  atomic 4
  extr 4

  addb rl4, rh4     
  addb rl4, [r1]
  addb rl4, [r1+]
  addb rl4, 7
  addb rl4, 0xFE
  addb %S0TBUF, 0xBE
  addb rl4, [0xFF12]
  addb [0xFF12], %S0TBUF

  addcb rl4, rh4     
  addcb rl4, [r1]
  addcb rl4, [r1+]
  addcb rl4, 7
  addcb rl4, 0xFE
  addcb %S0TBUF, 0xBE
  addcb rl4, [0xFF12]
  addcb [0xFF12], %S0TBUF

  andb rl4, rh4     
  andb rl4, [r1]
  andb rl4, [r1+]
  andb rl4, 7
  andb rl4, 0xFE
  andb %S0TBUF, 0xBE
  andb rl4, [0xFF12]
  andb [0xFF12], %S0TBUF

  orb rl4, rh4     
  orb rl4, [r1]
  orb rl4, [r1+]
  orb rl4, 7
  orb rl4, 0xFE
  orb %S0TBUF, 0xBE
  orb rl4, [0xFF12]
  orb [0xFF12], %S0TBUF

  subb rl4, rh4     
  subb rl4, [r1]
  subb rl4, [r1+]
  subb rl4, 7
  subb rl4, 0xFE
  subb %S0TBUF, 0xBE
  subb rl4, [0xFF12]
  subb [0xFF12], %S0TBUF

  subcb rl4, rh4     
  subcb rl4, [r1]
  subcb rl4, [r1+]
  subcb rl4, 7
  subcb rl4, 0xFE
  subcb %S0TBUF, 0xBE
  subcb rl4, [0xFF12]
  subcb [0xFF12], %S0TBUF

  xorb rl4, rh4     
  xorb rl4, [r1]
  xorb rl4, [r1+]
  xorb rl4, 7
  xorb rl4, 0xFE
  xorb %S0TBUF, 0xBE
  xorb rl4, [0xFF12]
  xorb [0xFF12], %S0TBUF

  cplb rl4
  negb rl4

  cmpb rl4, rh4     
  cmpb rl4, [r1]
  cmpb rl4, [r1+]
  cmpb rl4, 7
  cmpb rl4, 0xFE
  cmpb %S0TBUF, 0xBE
  cmpb rl4, [0xFF12]

  movb rl4, rh4
  movb rl4, 0xF
  movb rl4, 0xBE
  movb rl4, [r12]
  movb rl4, [r12+]
  movb [r12], rl4
  movb [-r12], rl4
  movb [r12], [r13]
  movb [r12+], [r13]
  movb [r12], [r13+]
  movb rl4, [r12 + 0xBEEF]
  movb [r12 + 0xBEEF], rl4
  movb [r12], [0xFF12]
  movb [0xFF12], [r12]
  movb rl4, [0xFF12]
  movb [0xFF12], %S0TBUF
}
