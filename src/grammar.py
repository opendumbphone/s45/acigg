grammar = """
    start: _item*

    _item: _pp_directive | block | _NEWLINE

    _pp_directive: pp_op_define
        | pp_op_namespace
        | pp_op_org

    pp_op_define: "@define" IDENT _expression
    pp_op_namespace: "@namespace" NAMESPACE
    pp_op_org: "@org" immediate

    block: block_head block_body

    block_head: BLK_SCOPE_QUALIFIER BLK_TYPE_QUALIFIER IDENT 
    block_body: "{" _line+ "}"

    BLK_SCOPE_QUALIFIER: "hidden" | "local" | "global"
    BLK_TYPE_QUALIFIER: "proc" | "data" | "label"

    _line: instruction? | local_label? | _NEWLINE

    local_label: "." IDENT ":"
    instruction: mnemotic [_expression [("," _expression)+]]
    mnemotic: IDENT

    _expression: register 
        | _wgpr_incdec
        | bitaddr 
        | immediate 
        | ccode
        | pointer 
        | infix_expression 
        | alias
        | link

    link: LABEL_REFERENCE
    alias: "$" IDENT

    infix_expression: _expression operator _expression

    pointer: "[" _expression "]"

    immediate: INT | REG_ADDR
    !operator: "+" | "-"

    # Condition codes
    ccode: /cc_[a-z]+/i

    # pre/postfix gpr expressions used in pointers
    _wgpr_incdec: wgpr_inc | wgpr_dec
    wgpr_inc: WGPR_INC
    wgpr_dec: WGPR_DEC

    WGPR_INC: WGPR "+"
    WGPR_DEC: "-" WGPR

    # Bitaddr
    bitaddr: register "." INT

    # Registers
    register: _gpr | REG

    _gpr: WGPR | BGPR
    WGPR: /R1[0-5]|R[0-9]/i
    BGPR: /R[LH][0-7]/i

    REG: "%" IDENT
    REG_ADDR: "&" IDENT

    # Integers
    INT: DEC_IMM | BIN_IMM | HEX_IMM

    DEC_IMM: DECIMAL_DIGIT+ 
    BIN_IMM: "0b" BINARY_DIGIT+
    HEX_IMM: "0x" HEX_DIGIT+

    BINARY_DIGIT: /[0-1]/
    DECIMAL_DIGIT: /[0-9]/
    HEX_DIGIT: /[0-9A-F]/i

    # Label references
    LABEL_REFERENCE: ("/"? BLOCK_NAME) | ("." IDENT)
    BLOCK_NAME: NAMESPACE
    NAMESPACE: IDENT ("::" IDENT)*
    
    # Identifiers
    IDENT.-1: /(?!R1[0-5]\W)(?!R[0-9]\W)(?!R[LH][0-7]\W)(?!CC_\w+)[a-zA-Z_][a-zA-Z0-9_]*/i
    %import common.CNAME

    # Skip parser
    COMMENT: /;[^\\n]*/
    %import common.NEWLINE -> _NEWLINE
    %import common.WS_INLINE
    %ignore WS_INLINE
    %ignore COMMENT
"""

ccodeLookup = {
    "UC": 0x0,
    "Z": 0x2,
    "NZ": 0x3,
    "V": 0x4,
    "NV": 0x5,
    "N": 0x6,
    "NN": 0x7,
    "C": 0x8,
    "NC": 0x9,
    "EQ": 0x2,
    "NE": 0x3,
    "NEQ": 0x3,
    "ULT": 0x8,
    "ULE": 0xF,
    "UGE": 0x9,
    "UGT": 0xE,
    "SLT": 0xC,
    "SLE": 0xB,
    "SGE": 0xD,
    "SGT": 0xA,
    "NET": 0x1
}
