from lark import Transformer, v_args
from lark import visitors
import encoders as EC

# Addressing modes (See page 6 in manual)
# For each mode all possible encoders are listed
instruction_arg_modes = [
    # Mode 0
    [
        EC.rwn_rwm, 
        EC.rwn_rwiPtr,
        EC.rwn_rwiIncPtr,
        EC.rwn_data3,
        EC.reg_data16,
        EC.reg_mem,
        EC.mem_reg
    ],
    
    # Mode 1
    [
        EC.rwn_rwm,
        EC.rwn_data4
    ],
    
    # Mode 2
    [
        EC.bitaddr_bitaddr 
    ],

    # Mode 3
    [
        EC.bitaddr
    ],

    # Mode 4
    [
        EC.bitoff_mask8_data8
    ],

    # Mode 5
    [
        EC.rwn_irang2,
        EC.seg_irang2
    ],

    # Mode 6
    [
        EC.noarg
    ],

    # Mode 7 
    [
        EC.rwn_zeropad
    ],

    # Mode 8 
    [
        EC.rwn_redundantpad
    ],

    # Mode 9
    [
        EC.rwn_rwm
    ],

    # Mode 10
    [
        EC.rwn_data4,
        EC.rwn_data16,
        EC.rwn_mem
    ],

    # Mode 11
    [
        EC.rwn_rwm,
        EC.rwn_rwiPtr,
        EC.rwn_rwiIncPtr,
        EC.rwn_data3,
        EC.reg_data16,
        EC.reg_mem
    ],

    # Mode 12
    [
        EC.cc_linkAbs,
        EC.cc_caddr
    ],

    # Mode 13
    [
        EC.cc_rwnPtr
    ],

    # Mode 14
    [
        EC.rwn_irang2,
        EC.pag_irang2
    ],
    
    # Mode 15
    [
        EC.rwn_rwm,
        EC.rwn_data4,
        EC.reg_data16,
        EC.rwn_rwnPtr,
        EC.rwn_rwnIncPtr,
        EC.rwnPtr_rwn,
        EC.rwnDecPtr_rwn,
        EC.rwnPtr_rwnPtr,
        EC.rwnIncPtr_rwnPtr,
        EC.rwnPtr_rwnIncPtr,
        EC.rwn_rwnPlusOffsetPtr,
        EC.rwnPlusOffsetPtr_rwn,
        EC.rwnPtr_mem,
        EC.mem_rwnPtr,
        EC.reg_mem,
        EC.mem_reg
    ],

    # Mode 16
    [
        EC.rwn_rbn,
        EC.reg_mem,
        EC.mem_regb,
    ],

    # Mode 17
    [
        EC.linkSeg,
        EC.segcaddr24, # Handles 24-bit immediates
        EC.seg_caddr
    ],

    # Mode 18
    [
        EC.linkRel,
        EC.rel
    ],

    # Mode 19
    [
        EC.cc_linkRel,
        EC.cc_rel
    ],

    # Mode 20
    [
        EC.bitaddr_linkRel,
        EC.bitaddr_rel
    ],

    # Mode 21
    [
        EC.reg_linkAbs,
        EC.reg_caddr
    ],

    # Mode 22
    [
        EC.reg
    ],

    # Mode 23
    [
        EC.reg_data16,
        EC.reg_mem
    ],

    # Mode 24
    [
        EC.rwn_rwm
    ],

    # Mode 25
    [
        EC.trap7
    ],

    # Mode 26
    [
        EC.irang2
    ],

    # Byte modes

    # Mode 27
    [
        EC.rbn_rbm,
        EC.rbn_rwiPtr,
        EC.rbn_rwiIncPtr,
        EC.rbn_data3,
        EC.regb_data8,
        EC.regb_mem,
        EC.mem_regb
    ],

    # Mode 28
    [
        EC.rbn
    ],

    # Mode 29
    [
        EC.rbn_rbm,
        EC.rbn_rwiPtr,
        EC.rbn_rwiIncPtr,
        EC.rbn_data3,
        EC.regb_data8,
        EC.regb_mem,
    ],

    # Mode 30
    [
        EC.rbn_rbm,
        EC.rbn_data4,
        EC.regb_data8,
        EC.rbn_rwnPtr,
        EC.rbn_rwnIncPtr,
        EC.rwnPtr_rbn,
        EC.rwnDecPtr_rbn,
        EC.rwnPtr_rwnPtr,
        EC.rwnIncPtr_rwnPtr,
        EC.rwnPtr_rwnIncPtr,
        EC.rbn_rwnPlusOffsetPtr,
        EC.rwnPlusOffsetPtr_rbn,
        EC.rwnPtr_mem,
        EC.mem_rwnPtr,
        EC.regb_mem,
        EC.mem_regb
    ],

    # Mode 31
    [
        EC.bitoff_mask8_data8_flipped
    ],
]

# For each mnemotic (instruction_arg_mode, [corresponding opcodes])
mnemotics = {
        # Mode 0
        "ADD": (0, [0x00, 0x08, 0x08, 0x08, 0x06, 0x02, 0x04]),
        "ADDC": (0, [0x10, 0x18, 0x18, 0x18, 0x16, 0x12, 0x14]),
        "AND": (0, [0x60, 0x68, 0x68, 0x68, 0x66, 0x62, 0x64]),
        "OR": (0, [0x70, 0x78, 0x78, 0x78, 0x76, 0x72, 0x74]),
        "SUB": (0, [0x20, 0x28, 0x28, 0x28, 0x26, 0x22, 0x24]),
        "SUBC": (0, [0x30, 0x38, 0x38, 0x38, 0x36, 0x32, 0x34]),
        "XOR": (0, [0x50, 0x58, 0x58, 0x58, 0x56, 0x52, 0x54]),

        # Mode 1
        "ASHR": (1, [0xAC, 0xBC]),
        "ROL": (1, [0x0C, 0x1C]),
        "ROR": (1, [0x2C, 0x3C]),
        "SHL": (1, [0x4C, 0x5C]),
        "SHR": (1, [0x6C, 0x6C]),

        # Mode 2
        "BAND": (2, [0x6A]),
        "BCMP": (2, [0x2A]),
        "BMOV": (2, [0x4A]),
        "BMOVN": (2, [0x3A]),
        "BOR": (2, [0x5A]),
        "BXOR": (2, [0x7A]),

        # Mode 3
        "BCLR": (3, [0x0E]),
        "BSET": (3, [0x0F]),

        # Mode 4
        "BFLDH": (4, [0x1A]),
        
        # Mode 5
        "EXTS": (5, [
                bytearray([0xDC, 0b00 << 6]),
                bytearray([0xD7, 0b00 << 6]),
                ]),
        "EXTSR": (5, [
                bytearray([0xDC, 0b10 << 6]),
                bytearray([0xD7, 0b10 << 6]),
                ]),

        # Mode 6
        "NOP": (6, [bytearray([0xCC, 0x00])]),
        "RET": (6, [bytearray([0xCB, 0x00])]),
        "RETI": (6, [bytearray([0xFB, 0x88])]),
        "RETS": (6, [bytearray([0xDB, 0x00])]),
        "SRST": (6, [bytearray([0xB7, 0x48, 0xB7, 0xB7])]),
        "IDLE": (6, [bytearray([0x87, 0x78, 0x87, 0x87])]),
        "PWRDN": (6, [bytearray([0x97, 0x68, 0x97, 0x97])]),
        "SRVWDT": (6, [bytearray([0xA7, 0x58, 0xA7, 0xA7])]),
        "DISWDT": (6, [bytearray([0xA5, 0x5A, 0xA5, 0xA5])]),
        "EINIT": (6, [bytearray([0xB5, 0x4A, 0xB5, 0xB5])]),

        # Mode 7
        "CPL": (7, [0x91]),
        "NEG": (7, [0x81]),

        # Mode 8
        "DIV": (8, [0x4B]),
        "DIVL": (8, [0x6B]),
        "DIVLU": (8, [0x7B]),
        "DIVU": (8, [0x5B]),

        # Mode 9
        "MUL": (9, [0x0B]),
        "MULU": (9, [0x1B]),

        # Mode 10
        "CMPD1": (10, [0xA0, 0xA6, 0xA2]),
        "CMPD2": (10, [0xB0, 0xB6, 0xB2]),
        "CMPI1": (10, [0x80, 0x86, 0x82]),
        "CMPI2": (10, [0x90, 0x96, 0x92]),

        # Mode 11
        "CMP": (11, [0x40, 0x48, 0x48, 0x48, 0x46, 0x42]),
        # "CMPB": (11, [0x41, 0x49, 0x49, 0x49, 0x47, 0x43]),

        # Mode 12
        "CALLA": (12, [0xCA, 0xCA]),
        "JMPA": (12, [0xEA, 0xEA]),

        # Mode 13
        "CALLI": (13, [0xAB]),
        "JMPI": (13, [0x9C]),

        # Mode 14
        "EXTP": (14, [
                bytearray([0xDC, 0b01 << 6]),
                bytearray([0xD7, 0b01 << 6]),
                ]),
        "EXTPR": (14, [
                bytearray([0xDC, 0b11 << 6]),
                bytearray([0xD7, 0b11 << 6]),
                ]),

        # Mode 15
        "MOV": (15, [0xF0, 0xE0, 0xE6, 0xA8, 0x98, 0xB8, 0x88, 0xC8, 0xD8, 0xE8, 0xD4, 0xC4, 0x84, 0x94, 0xF2, 0xF6]),

        # Mode 16
        "MOVBS": (16, [0xD0, 0xD2, 0xD5]),
        "MOVBZ": (16, [0xC0, 0xC2, 0xC5]),

        # Mode 17
        "CALLS": (17, [0xDA, 0xDA, 0xDA]),
        "JMPS": (17, [0xFA, 0xFA, 0xFA]),

        # Mode 18
        "CALLR": (18, [0xBB, 0xBB]),

        # Mode 19
        "JMPR": (19, [0x0D, 0x0D]),

        # Mode 20
        "JB": (20, [0x8A, 0x8A]),
        "JBC": (20, [0xAA, 0xAA]),
        "JNB": (20, [0x9A, 0x9A]),
        "JNBS": (20, [0xBA, 0xBA]),

        # Mode 21
        "PCALL": (21, [0xE2, 0xE2]),

        # Mode 22
        "POP": (22, [0xFC]),
        "PUSH": (22, [0xEC]),
        "RETP": (22, [0xEB]),

        # Mode 23
        "SCXT": (23, [0xC6, 0xD6]),

        # Mode 24
        "PRIOR": (24, [0x2B]),

        # Mode 25
        "TRAP": (25, [0x9B]),

        # Mode 26
        "ATOMIC": (26, [bytearray([0xD1, 0b00 << 6])]),
        "EXTR": (26, [bytearray([0xD1, 0b10 << 6])]),

        ## Byte modes

        # Mode 27
        "ADDB": (27, [0x01, 0x09, 0x09, 0x09, 0x07, 0x03, 0x05]),
        "ADDCB": (27, [0x11, 0x19, 0x19, 0x19, 0x17, 0x13, 0x15]),
        "ANDB": (27, [0x61, 0x69, 0x69, 0x69, 0x67, 0x63, 0x65]),
        "ORB": (27, [0x71, 0x79, 0x79, 0x79, 0x77, 0x73, 0x75]),
        "SUBB": (27, [0x21, 0x29, 0x29, 0x29, 0x27, 0x23, 0x25]),
        "SUBCB": (27, [0x31, 0x39, 0x39, 0x39, 0x37, 0x33, 0x35]),
        "XORB": (27, [0x51, 0x59, 0x59, 0x59, 0x57, 0x53, 0x55]),
        
        # Mode 28
        "CPLB": (28, [0xB1]),
        "NEGB": (28, [0xA1]),

        # Mode 29
        "CMPB": (29, [0x41, 0x49, 0x49, 0x49, 0x47, 0x43]),

        # Mode 30
        "MOVB": (30, [0xF1, 0xE1, 0xE7, 0xA9, 0x99, 0xB9, 0x89, 0xC9, 0xD9, 0xE9, 0xF4, 0xE4, 0xA4, 0xB4, 0xF3, 0xF7]),

        # Mode 31
        # Note: I have absolutely no idea why the arguments are flipped here
        "BFLDL": (31, [0x0A]),
        }

@v_args(inline=True)
class AssemblerTransformer(Transformer):

    def __init__(self):
        self.instruction_counter = 0
        self.linker_table = []

    # def line(self, *args):
    #     if len(list(args)) == 0:
    #         return visitors.Discard
    #     return args

    def _add_link_from_current(self, target: str, link_type: int):
        self.linker_table.append((self.instruction_counter, target, link_type))

    def instruction(self, mnemotic, *args):
        argmodes = instruction_arg_modes[mnemotics[mnemotic][0]]
        opcodes = mnemotics[mnemotic][1]

        encoded = None

        # Process instruction using regular encoders
        # For the mnemotic given, each regular encoder is examined.
        # If one returns sucessfully, it will have produced a correct
        # and optimally small encoding of the instruction
        for encoder, opcode in zip(argmodes, opcodes):
            try:
                print("calling", encoder, "on", opcode, args)
                encoded = encoder(opcode, *args, assembler=self)
                print("Returned", encoded)
            except AssertionError:
                pass

            if encoded is not None:
                break

        if encoded is None:
            raise Exception("No encoder for arguments")

        self.instruction_counter += 1
        return encoded
        # return mnemotic, list(args)

    def mnemotic(self, mnem, *args):
        mnem = mnem.upper()
        if mnem in mnemotics.keys():
            return mnem

        raise Exception

    # def register(self, token):
    #     return token
    # def register(self, args):
    #     print(args.type, args.value)


    @v_args(inline=False)
    def expression(self, args):
        if len(args) == 1:
            return args[0]
        else:
            return __default__(args)

