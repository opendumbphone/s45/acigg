import sys
import binascii
from lark import Lark, Transformer
from lark.visitors import Visitor

from grammar import grammar
from assembler import AssemblerTransformer
from preprocessor import PreprocessorTransformer
from linker import Linker

class OutputGen(Visitor):
    def __init__(self):
        self.binary = bytearray()

    def fetch(self):
        return self.binary

    def start(self, tree):
        for i in tree.children:
            print(binascii.hexlify(i))
            self.binary.extend(i)


def assemble(code):
    # Parser
    parser = Lark(grammar, parser='lalr')
    parsetree = parser.parse(code)
    print(parsetree.pretty())
    print(parsetree)

    # Preprocessor
    preprocessor = PreprocessorTransformer()
    working_tree = preprocessor.transform(parsetree)
    print("\n>>>PREPROCESSOR RETURNED")
    print(working_tree.pretty())
    print("Label table: ", preprocessor.labels)
    print("Linker table:", preprocessor.linker_table)
    
    # Assembler
    assembler = AssemblerTransformer()
    working_tree = assembler.transform(working_tree)
    print(working_tree.pretty())
    print(working_tree)

    print("Label table: ", preprocessor.labels)
    print("LINKER:")
    # Linker
    linker = Linker(preprocessor.labels, assembler.linker_table, preprocessor.org)
    linker.link(working_tree)
    print(working_tree.pretty())

    # Output
    outputgen = OutputGen()
    outputgen.visit(working_tree)
    return outputgen.fetch()

    
def write_binary_file(filename, binary_code):
    with open(filename, 'wb') as f:
        f.write(binary_code)

if __name__ == '__main__':
    with open(sys.argv[1], 'r') as file:
        assembly_code = file.read()
        binary_code = assemble(assembly_code)

    print(binascii.hexlify(binary_code))
    write_binary_file("output.bin", binary_code)
    print("Binary code written to output.bin")
