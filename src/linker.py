
# Performs linking on a working tree that only consists of bytearrays
# (output of the assembler stage)
# Linker table must be a list of tuples: (instruction_no, dest_label, link_type)
# Label dictionary must map from full label to instruction_no
class Linker():
    def __init__(self, labels, links, org=0x0):
        self.labels = labels
        self.links = links
        self.org = org
        self.instruction_addresses = []
        self.working_tree = None

        print("Linker received labels", labels)
        print("Linking against", links)

    def relative_link(self, instruction_no, dest_addr, destination_byte=1, instruction_length=2):
        delta = dest_addr - (self.instruction_addresses[instruction_no] + instruction_length)
        delta //= 2

        # Adjust for 2's complement on negative jumps
        if delta < 0:
            delta = 0xFF + delta + 1

        try:
            self.working_tree.children[instruction_no][destination_byte] = delta
        except ValueError:
            print("Relative jump gap too big")
            exit(1)
            # TODO: Error handling


    def link(self, tree):
        self.working_tree = tree # Save tree reference for access in other functions

        # 1. Traverse through the tree and get the offset of all instructions
        offset = self.org

        for instruction in tree.children:
            self.instruction_addresses.append(offset)
            offset += len(instruction)

        # 2. Process all links in the linker table
        print(self.labels)
        for instruction_no, dest_label, link_type in self.links:

            # Get 24bit (full) target address
            try:
                dest_addr = self.instruction_addresses[self.labels[dest_label]]
            except KeyError:
                # TODO: Error handling
                print("No Label called", dest_label)
                exit(1)

            print("Linking", instruction_no, dest_label, link_type)
            match link_type:
                # REL (relative link)
                case 0:
                    self.relative_link(instruction_no, dest_addr)

                # REL_BITADDR
                case 3:
                    self.relative_link(instruction_no, dest_addr, 2, 4)

                # CADDR (absolute link)
                case 1:
                    val = dest_addr & 0xFFFF
                    print(tree.children[instruction_no])
                    tree.children[instruction_no][2] = (val & 0x0000FF)
                    tree.children[instruction_no][3] = (val & 0x00FF00) >> 8

                # SEG, CADDR (segmented link)
                case 2:
                    val = dest_addr
                    tree.children[instruction_no][1] = (val & 0xFF0000) >> 16
                    tree.children[instruction_no][2] = (val & 0x0000FF)
                    tree.children[instruction_no][3] = (val & 0x00FF00) >> 8

                case _:
                    print("Illegal Link type, this should not happen, received type", link_type)
                    exit(1)

