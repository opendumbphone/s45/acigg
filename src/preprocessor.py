from lark import Transformer, v_args
from lark import visitors
from lark import Visitor
from lark.tree import Tree
from lark.lexer import Token
from lark.visitors import Transformer_InPlace

LINK_REL = 0
LINK_ABS = 1
LINK_SEG = 2

# Returns (scope, type, identifier) for a block
def get_block_metadata(block_head: Tree) -> tuple:
    head = block_head.children
    scope = head[0]
    block_type = head[1]
    identifier = str(head[2])

    return scope, block_type, identifier

@v_args(inline=True)
class PreprocessorTransformer(Transformer):
    def __init__(self):
        Transformer.__init__(self)
        self.namespace = "default"
        self.labels = {}
        self.instruction_counter = 0
        self.linker_table = []
        self.org = 0
    
    def pp_op_namespace(self, *args):
        self.namespace = args[0]
        return visitors.Discard

    def pp_op_org(self, *args):
        assert self.instruction_counter == 0 # Org may only be called at the beginning of a file
        self.org = int(args[0].children[0], 0)
        return visitors.Discard

    @v_args(tree=True)
    def block(self, blockTree):
        head = blockTree.children[0]
        body = blockTree.children[1]
        assert len(body.children) != 0 # TODO: Error handling on empty block

        # TODO: Actually validate meta info and put into linking table
        # Or maybe just ignore it and keep it as decoration
        meta = get_block_metadata(head)
        identifier = meta[2]

        # Name used to globally identify block label
        full_block_name = self.namespace + "::" + identifier

        # Put link target into table
        self._add_label(full_block_name, self.instruction_counter)

        # Process all local labels accordingly and adjust instruction counter
        link_prep = LinkPreparer(self.namespace, full_block_name)
        for tree in body.children:
            # Register labels for local labels
            if tree.data == "local_label":
                full_label_name = full_block_name + "." + str(tree.children[0])
                self._add_label(full_label_name, self.instruction_counter)
            
            # Prepare links for instructions
            else:
                link_prep.transform(tree)
                self.instruction_counter += 1

        # Only return the block body
        return body

    # At this point, only block bodies should be left in the tree.
    # We adjust the tree such that each child of the start node is transformable
    # by the assembler (only instructions)
    @v_args(tree=True)
    def start(self, startTree):
        asm_primitives = []
        for block_body in startTree.children:
            for tree in block_body.children:
                if tree.data == "instruction":
                    asm_primitives.append(tree)

        return Tree(startTree.data, asm_primitives, startTree.meta)

    def _add_label(self, name, instruction_no):
        self.labels[name] = instruction_no

    def _add_link(self, _from: int, to: str, link_type: int):
        self.linker_table.append((_from, to, link_type))

# Goes through an instruction tree and replaces all label references in links with their respective
# full target labels (i.e. with namespaces and all that)
@v_args(tree=True)
class LinkPreparer(Transformer_InPlace):
    def __init__(self, namespace: str, block_name: str):
        self.namespace = namespace
        self.block_name = block_name
        Transformer_InPlace.__init__(self)

    def _get_full_target_name(self, target):
        short_target_name = str(target)
        print(short_target_name)
        match short_target_name[0]:
            # Local labels start with a dot and resolve to namespace::block.local
            case '.':
                return self.block_name + short_target_name
            
            # Global label targets specify the entire hierarchy from bottom up.
            # They start with / and resolve to the given target
            case '/':
                return short_target_name[1:]

            # Labels of blocks within the same namespace start without any special character and resolve to namespace::block
            case _:
                return self.namespace + "::" + short_target_name

    def link(self, tree):
        return Tree(Token('RULE', 'link'), [Token('LABEL_REFERENCE', self._get_full_target_name(tree.children[0]))])

