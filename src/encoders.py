from lark.tree import Tree
from lark.lexer import Token
from registers import reglookup, regAddrLookup
from grammar import ccodeLookup

# === Linker things ===
LINK_REL = 0
LINK_ABS = 1
LINK_SEG = 2
LINK_BITADDR_REL = 3

# === Tree transformers ===
"""
Tries to encode an immediate pointer tree to a mem type bytearray
"""
def immPtr_encode(tree: Tree) -> int:
    # Pointer tree
    assert tree.data == "pointer"
    assert len(tree.children) == 1
    
    # Immediate Tree
    tree = tree.children[0]
    assert type(tree) == Tree
    assert tree.data == "immediate"

    # Immediate Token
    imm = tree.children[0]
    assert type(imm) == Token
    match imm.type:
        case "INT":
            return imm_to_data16(imm)
        case "REG_ADDR":
            return endianswap16(regAddrLookup(imm[1:]))

"""
Tries to encode a register tree to a reg type integer
"""
def reg_encode(tree: Tree) -> int:
    assert tree.data == "register"
    token = tree.children[0]

    reg = None
    match token.type:
        case "REG":
            reg = reglookup(token[1:])
        case "WGPR":
            reg = 0xF0 + wgpr_to_rwn(token)
        case _:
            raise AssertionError

    return reg

"""
Tries to encode a register tree to an rwn type integer
"""
def rwn_encode(tree: Tree) -> int:
    assert tree.data == "register"
    assert tree.children[0].type == "WGPR"

    return wgpr_to_rwn(tree.children[0])

"""
Tries to encode a pointer tree to an rwi type integer
"""
def rwiPtr_encode(tree: Tree) -> int:
    assert tree.data == "pointer"
    assert len(tree.children) == 1
    assert type(tree.children[0]) == Tree
    assert tree.children[0].data == "register"
    assert tree.children[0].children[0].type == "WGPR"

    return wgpr_to_rwi(tree.children[0].children[0])

"""
Tries to encode a pointer tree (with a WGPR_INC) to an rwi type integer
"""
def rwiIncPtr_encode(tree: Tree) -> int:
    # pointer tree
    assert tree.data == "pointer"
    assert len(tree.children) == 1
    
    # WGPR increment tree
    tree = tree.children[0]
    assert type(tree) == Tree
    assert tree.data == "wgpr_inc"
    assert type(tree.children[0]) == Token
    assert tree.children[0].type == "WGPR_INC"

    return wgpr_to_rwi(tree.children[0][:-1])

"""
Tries to encode an immediate tree to an integer
"""
def imm_encode(tree: Tree, bits: int = None) -> int:
    assert tree.data == "immediate"
    assert len(tree.children) == 1
    assert type(tree.children[0]) == Token
    assert tree.children[0].type == "INT"

    val = int(tree.children[0], 0)
    if bits is not None:
        assert val < (1 << bits)

    return val

"""
Tries to encode a bitaddr tree to a tuple of (reg, offset)
"""
def bitaddr_encode(tree: Tree) -> tuple:
    assert tree.data == 'bitaddr'
    reg = reg_encode(tree.children[0])
    off = imm_to_data4(tree.children[1])
    return (reg, off)

"""
Tries to encode a register tree to a bitoff (bitaddressable reg)
"""
def bitoff_encode(tree: Tree) -> int:
    reg = reg_encode(tree)
    # TODO: Check here if register is addressable
    return reg

"""
Tries to encode an immediate tree to an irang integer
"""
def irang_encode(tree: Tree) -> int:
    irang = imm_encode(tree) - 1
    assert irang < 4
    return irang

"""
Tries to encode an immediate tree to a caddr integer
"""
def caddr_encode(tree: Tree) -> int:
    addr = imm_encode(tree, 16)
    # TODO: Proper error message here
    assert addr % 2 == 0
    return endianswap16(addr)

"""
Tries to encode a condition code tree to a cc integer
"""
def cc_encode(tree: Tree) -> int:
    assert tree.data == "ccode"
    res = 0
    try:
        res = ccodeLookup[tree.children[0][3:].upper()]
    except KeyError:
        raise AssertionError("Invalid Condition code") 

    return res

"""
Tries to encode a pointer tree with a WGPR to an rwn integer
"""
def rwnPtr_encode(tree: Tree) -> int:
    assert tree.data == "pointer"
    assert len(tree.children) == 1
    assert type(tree.children[0]) == Tree
    return rwn_encode(tree.children[0])

"""
Tries to encode a pointer tree with a wgpr_inc to an rwn integer
"""
def rwnIncPtr_encode(tree: Tree) -> int:
    # pointer tree
    assert tree.data == "pointer"
    assert len(tree.children) == 1
    
    # WGPR increment tree
    tree = tree.children[0]
    assert type(tree) == Tree
    assert tree.data == "wgpr_inc"
    assert type(tree.children[0]) == Token
    assert tree.children[0].type == "WGPR_INC"

    return wgpr_to_rwn(tree.children[0][:-1])

"""
Tries to encode a pointer tree with a wgpr_dec to an rwn integer
"""
def rwnDecPtr_encode(tree: Tree) -> int:
    # pointer tree
    assert tree.data == "pointer"
    assert len(tree.children) == 1
    
    # WGPR increment tree
    tree = tree.children[0]
    assert type(tree) == Tree
    assert tree.data == "wgpr_dec"
    assert type(tree.children[0]) == Token
    assert tree.children[0].type == "WGPR_DEC"

    return wgpr_to_rwn(tree.children[0][1:])


"""
Tries to encode a pointer tree with a wgpr + data16 to a tuple (rwn, data)
"""
def rwnPlusOffsetPtr_encode(tree: Tree) -> tuple:
    print(tree)
    # pointer tree
    assert tree.data == "pointer"
    assert len(tree.children) == 1

    # Infix expression
    tree = tree.children[0]
    assert type(tree) == Tree
    assert tree.data == "infix_expression"

    # Expression arguments
    assert tree.children[1].data == "operator"
    assert tree.children[1].children[0] == "+"

    rwn = rwn_encode(tree.children[0])
    data = endianswap16(imm_encode(tree.children[2], 16))
    return (rwn, data)

"""
Tries to encode an integer (signed decimal / unsigned hex) to a relative jump offset type integer
"""
# 8 bit unsigned interpreted as signed word offset
def rel_encode(tree: Tree) -> int:
    return imm_encode(tree, 8)

"""
Tries to encode a bpgr register tree to an rbn type integer
"""
def rbn_encode(tree: Tree) -> int:
    assert tree.data == "register"
    assert tree.children[0].type == "BGPR"
    return bgpr_to_rbn(tree.children[0])

"""
Tries to encode a register tree to a byte reg type integer
"""
def regb_encode(tree: Tree) -> int:
    assert tree.data == "register"
    token = tree.children[0]

    reg = None
    match token.type:
        case "REG":
            reg = reglookup(token[1:])
        case "BGPR":
            reg = 0xF0 + bgpr_to_rbn(token)
        case _:
            raise AssertionError

    return reg

# === Encoding helpers ===
def wgpr_to_rwn(wgpr: str) -> int:
    return int(wgpr[1:])

def bgpr_to_rbn(bgpr: str) -> int:
    return (int(bgpr[2]) * 2) + (bgpr[1].upper() == "H")

def wgpr_to_rwi(wgpr: str) -> int:
    i = int(wgpr[1:])
    assert i <= 3
    return i

def imm_to_data4(imm: str) -> int:
    data = int(imm, 0)
    assert data <= 0xF
    return data

def imm_to_data3(imm: str) -> int:
    data = int(imm, 0)
    assert data <= 3
    return data

def imm_to_data16(imm: str) -> bytearray:
    val = int(imm, 0)
    assert val <= 0xFFFF
    return endianswap16(val)

def endianswap16(data: int) -> bytearray:
    return bytearray([(data & 0xFF), (data & 0xFF00) >> 8])

# Tree structure check
def correct_structure(args, no_args: int, types: list) -> bool:
    if len(args) != no_args:
        return False
    
    for idx, _type in enumerate(types):
        if type(args[idx]) != _type:
            return False

    return True

# === Instruction encoders ===
# OP rwn, rwm
def rwn_rwm(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rwn_encode(args[0])
    m = rwn_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP rwn, [rwi]
def rwn_rwiPtr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rwn_encode(args[0])
    i = rwiPtr_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | 0b1000 | i
    return res
    
# OP rwn, [rwi+]
def rwn_rwiIncPtr(opcode: int, *args, **kwargs):
    assert len(args) == 2
    n = rwn_encode(args[0])
    i = rwiIncPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | 0b1100 | i
    return res

# OP rwn, data3
def rwn_data3(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rwn_encode(args[0])
    data = imm_encode(args[1], 3)
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | data
    return res

# OP reg, data16
def reg_data16(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    reg = reg_encode(args[0])
    imm = imm_encode(args[1], 16)
    data = endianswap16(imm)

    res = bytearray(2)
    res[0] = opcode
    res[1] = reg
    res.extend(data)
    return res
    
# OP reg, mem
def reg_mem(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    reg = reg_encode(args[0])
    mem = immPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = reg
    res.extend(mem)
    return res

# Op mem, reg
def mem_reg(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    mem = immPtr_encode(args[0])
    reg = reg_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = reg
    res.extend(mem)
    return res

# OP rwn, data4
def rwn_data4(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])

    n = rwn_encode(args[0])
    data = imm_encode(args[1], 4)

    res = bytearray(2)
    res[0] = opcode
    res[1] = (data << 4) | n
    return res

# OP bitaddr, bitaddr
def bitaddr_bitaddr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    bitAddrZ = bitaddr_encode(args[0])
    bitAddrQ = bitaddr_encode(args[1])

    res = bytearray(4)
    res[0] = opcode
    res[1] = bitAddrQ[0]
    res[2] = bitAddrZ[0]
    res[3] = (bitAddrQ[1] << 4) | bitAddrZ[1]

    return res;

# OP bitaddr
def bitaddr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 1, [Tree])
    bitAddrQ = bitaddr_encode(args[0])

    res = bytearray(2)
    res[0] = (bitAddrQ[1] << 4) | opcode
    res[1] = bitAddrQ[0]

    return res

# OP bitoff, mask8, data8
def bitoff_mask8_data8(opcode: int, *args, **kwargs):
    assert correct_structure(args, 3, [Tree, Tree, Tree])

    reg = bitoff_encode(args[0])
    mask = imm_encode(args[1], 8)
    data = imm_encode(args[2], 8)

    res = bytearray(4)
    res[0] = opcode
    res[1] = reg
    res[2] = data
    res[3] = mask

    return res

# This might just be an error in the manual, but we gotta assume
# it's true
# OP bitoff, mask8, data8 (for BFLDL)
def bitoff_mask8_data8_flipped(opcode: int, *args, **kwargs):
    assert correct_structure(args, 3, [Tree, Tree, Tree])

    reg = bitoff_encode(args[0])
    mask = imm_encode(args[1], 8)
    data = imm_encode(args[2], 8)

    res = bytearray(4)
    res[0] = opcode
    res[1] = reg
    res[3] = data
    res[2] = mask

    return res
# OP rwn, irang2
# Note: Opcode is bytearray
def rwn_irang2(opcode, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    rwn = rwn_encode(args[0])
    irang = irang_encode(args[1])

    res = bytearray(2)
    res[0] = opcode[0]
    res[1] = opcode[1] | (irang << 4) | rwn

    return res

# OP seg, irang2
# Note: Opcode is bytearray
def seg_irang2(opcode, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    seg = imm_encode(args[0], 8)
    irang = irang_encode(args[1])
    
    res = bytearray(4)
    res[0] = opcode[0]
    res[1] = opcode[1] | (irang << 4)
    res[2] = seg
    res[3] = 0x00

    return res

# OP without arg
def noarg(opcode, *args, **kwargs):
    assert len(args) == 0
    return opcode

# OP rwn (zero padding)
def rwn_zeropad(opcode: int, *args, **kwargs):
    assert correct_structure(args, 1, [Tree])
    rwn = rwn_encode(args[0])

    res = bytearray(2)
    res[0] = opcode
    res[1] = (rwn << 4)

    return res

# OP rwn (redundant padding)
def rwn_redundantpad(opcode: int, *args, **kwargs):
    assert correct_structure(args, 1, [Tree])
    rwn = rwn_encode(args[0])

    res = bytearray(2)
    res[0] = opcode
    res[1] = (rwn << 4) | rwn

    return res

# OP rwn, data16
def rwn_data16(opcode: int, *args, **kwargs):
    assert correct_structure(args,2, [Tree, Tree])
    rwn = rwn_encode(args[0])
    data = endianswap16(imm_encode(args[1], 16))

    res = bytearray(2)
    res[0] = opcode
    res[1] = 0xF0 | rwn
    res.extend(data)

    return res

# OP rwn, [mem]
def rwn_mem(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    rwn = rwn_encode(args[0])
    mem = immPtr_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = 0xF0 | rwn
    res.extend(mem)

    return res

def cc_linkAbs(opcode: int, *args, **kwargs):
    assert len(args) == 2
    assert args[1].data == "link"
    asm = kwargs["assembler"]

    cc = cc_encode(args[0])

    name = str(args[1].children[0])

    asm._add_link_from_current(name, LINK_ABS)

    res = bytearray(4)
    res[0] = opcode
    res[1] = cc << 4
    res[2] = 0
    res[3] = 0

    return res

# OP cc, caddr
def cc_caddr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    cc = cc_encode(args[0])
    caddr = caddr_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = cc << 4
    res.extend(caddr)

    return res

# OP cc, [rwn]
def cc_rwnPtr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    cc = cc_encode(args[0])
    rwn = rwnPtr_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = (cc << 4) | rwn

    return res
    
# OP pag, irang2
def pag_irang2(opcode, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    pag = endianswap16(imm_encode(args[0], 10))
    irang = irang_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode[0]
    res[1] = opcode[1] | (irang << 4)
    res.extend(pag)

    return res

# OP rwn, [rwm]
def rwn_rwnPtr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rwn_encode(args[0])
    m = rwnPtr_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP rwn, [rwm+]
def rwn_rwnIncPtr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rwn_encode(args[0])
    m = rwnIncPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP [rwm], rwn
def rwnPtr_rwn(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    m = rwnPtr_encode(args[0])
    n = rwn_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP [-rwm], rwn
def rwnDecPtr_rwn(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    m = rwnDecPtr_encode(args[0])
    n = rwn_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP [rwn], [rwm]
def rwnPtr_rwnPtr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rwnPtr_encode(args[0])
    m = rwnPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP [rwn+], [rwm]
def rwnIncPtr_rwnPtr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rwnIncPtr_encode(args[0])
    m = rwnPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP [rwn], [rwm+]
def rwnPtr_rwnIncPtr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rwnPtr_encode(args[0])
    m = rwnIncPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP rwn, [rwm + data16]
def rwn_rwnPlusOffsetPtr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rwn_encode(args[0])
    m, offset = rwnPlusOffsetPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    res.extend(offset)
    return res

# OP [rwm + data16], rwn
def rwnPlusOffsetPtr_rwn(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    m, offset = rwnPlusOffsetPtr_encode(args[0])
    n = rwn_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    res.extend(offset)
    return res

# OP [rwn], [mem]
def rwnPtr_mem(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    rwn = rwnPtr_encode(args[0])
    mem = immPtr_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = rwn
    res.extend(mem)

    return res

# OP [mem], [rwn]
def mem_rwnPtr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    mem = immPtr_encode(args[0])
    rwn = rwnPtr_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = rwn
    res.extend(mem)

    return res

def rwn_rbn(opcode: int, *args, **kwargs):
    assert len(args) == 2
    rwn = rwn_encode(args[0])
    rbm = rbn_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = (rbm << 4) | rwn

    return res

def regb_mem(opcode: int, *args, **kwargs):
    assert len(args) == 2
    regb = regb_encode(args[0])
    mem = immPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = regb
    res.extend(mem)

    return res

def mem_regb(opcode: int, *args, **kwargs):
    assert len(args) == 2
    mem = immPtr_encode(args[0])
    regb = regb_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = regb
    res.extend(mem)

    return res

def linkSeg(opcode: int, *args, **kwargs):
    assert len(args) == 1
    assert args[0].data == "link"
    asm = kwargs["assembler"]
    name = str(args[0].children[0])

    asm._add_link_from_current(name, LINK_SEG)
    
    res = bytearray(4)
    res[0] = opcode
    res[1] = 0
    res[2] = 0
    res[3] = 0
    return res
    
def segcaddr24(opcode: int, *args, **kwargs):
    assert len(args) == 1
    imm = imm_encode(args[0], 24)
    
    res = bytearray(4)
    res[0] = opcode
    res[1] = (imm & 0xFF0000) >> 16
    res[2] = (imm & 0x0000FF)
    res[3] = (imm & 0x00FF00) >> 8
    return res

def seg_caddr(opcode: int, *args, **kwargs):
    assert len(args) == 2
    seg = imm_encode(args[0], 8)
    caddr = endianswap16(imm_encode(args[1], 16))
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = seg
    res.extend(caddr)

    return res

def rel(opcode: int, *args, **kwargs):
    assert len(args) == 1
    data = rel_encode(args[0])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = data
    return res

def linkRel(opcode: int, *args, **kwargs):
    assert len(args) == 1
    assert args[0].data == "link"
    asm = kwargs["assembler"]
    name = str(args[0].children[0])

    asm._add_link_from_current(name, LINK_REL)
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = 0
    return res

def cc_rel(opcode: int, *args, **kwargs):
    assert len(args) == 2
    cc = cc_encode(args[0])
    data = rel_encode(args[1])

    res = bytearray(2)
    res[0] = (cc << 4) | opcode
    res[1] = data
    return res

def cc_linkRel(opcode: int, *args, **kwargs):
    assert len(args) == 2
    asm = kwargs["assembler"]

    cc = cc_encode(args[0])

    assert args[1].data == "link"
    name = str(args[1].children[0])

    asm._add_link_from_current(name, LINK_REL)
    
    res = bytearray(2)
    res[0] = (cc << 4) | opcode
    res[1] = 0
    return res

def bitaddr_rel(opcode: int, *args, **kwargs):
    assert len(args) == 2
    bitaddr = bitaddr_encode(args[0])
    data = rel_encode(args[1])

    res = bytearray(4)
    res[0] = opcode
    res[1] = bitaddr[0]
    res[2] = data
    res[3] = bitaddr[1] << 4
    return res

def bitaddr_linkRel(opcode: int, *args, **kwargs):
    assert len(args) == 2
    asm = kwargs["assembler"]

    bitaddr = bitaddr_encode(args[0])

    assert args[1].data == "link"
    name = str(args[1].children[0])

    asm._add_link_from_current(name, LINK_BITADDR_REL)

    res = bytearray(4)
    res[0] = opcode
    res[1] = bitaddr[0]
    res[2] = 0
    res[3] = bitaddr[1] << 4
    return res

def reg_linkAbs(opcode: int, *args, **kwargs):
    assert len(args) == 2
    asm = kwargs["assembler"]

    reg = reg_encode(args[0])

    assert args[1].data == "link"
    name = str(args[1].children[0])

    asm._add_link_from_current(name, LINK_ABS)

    res = bytearray(4)
    res[0] = opcode
    res[1] = reg
    res[2] = 0
    res[3] = 0
    return res

def reg_caddr(opcode: int, *args, **kwargs):
    assert len(args) == 2
    reg = reg_encode(args[0])
    data = caddr_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = reg
    res.extend(data)
    return res

def reg(opcode: int, *args, **kwargs):
    assert len(args) == 1
    reg = reg_encode(args[0])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = reg
    return res

def trap7(opcode: int, *args, **kwargs):
    assert len(args) == 1
    trap = imm_encode(args[0], 7)

    res = bytearray(2)
    res[0] = opcode
    res[1] = trap << 1
    return res

# Opcode is bytearray
def irang2(opcode, *args, **kwargs):
    assert len(args) == 1
    i = irang_encode(args[0])

    res = bytearray(2)
    res[0] = opcode[0]
    res[1] = opcode[1] | (i << 4) | 0x0
    return res

def rbn_rbm(opcode: int, *args, **kwargs):
    assert len(args) == 2
    n = rbn_encode(args[0])
    m = rbn_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

def rbn_rwiPtr(opcode: int, *args, **kwargs):
    assert len(args) == 2
    n = rbn_encode(args[0])
    i = rwiPtr_encode(args[1])

    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | 0b1000 | i
    return res

def rbn_rwiIncPtr(opcode: int, *args, **kwargs):
    assert len(args) == 2
    n = rbn_encode(args[0])
    i = rwiIncPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | 0b1100 | i
    return res

# OP rbn, [rwi+]
def rbn_rwiIncPtr(opcode: int, *args, **kwargs):
    assert len(args) == 2
    n = rbn_encode(args[0])
    i = rwiIncPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | 0b1100 | i
    return res

# OP rbn, data3
def rbn_data3(opcode: int, *args, **kwargs):
    assert len(args) == 2
    n = rbn_encode(args[0])
    data = imm_encode(args[1], 3)
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | data
    return res

# OP reg, data8 (byte reg)
def regb_data8(opcode: int, *args, **kwargs):
    assert len(args) == 2
    regb = regb_encode(args[0])
    data = imm_encode(args[1], 8)

    res = bytearray(4)
    res[0] = opcode
    res[1] = regb
    res[2] = data
    res[3] = 0x0
    return res

def rbn(opcode: int, *args, **kwargs):
    assert len(args) == 1
    n = rbn_encode(args[0])

    res = bytearray(2)
    res[0] = opcode
    res[1] = n << 4
    return res

# OP rbn, data4
def rbn_data4(opcode: int, *args, **kwargs):
    assert len(args) == 2
    n = rbn_encode(args[0])
    data = imm_encode(args[1], 4)
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (data << 4) | n
    return res

# OP rbn, [rwm]
def rbn_rwnPtr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rbn_encode(args[0])
    m = rwnPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP rbn, [rwm+]
def rbn_rwnIncPtr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rbn_encode(args[0])
    m = rwnIncPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP rbn, [rwm+]
def rbnPtr_rwn(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rbn_encode(args[0])
    m = rwnIncPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP [rwn], rbm
def rwnPtr_rbn(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    m = rwnPtr_encode(args[0])
    n = rbn_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP [-rwn], rbm
def rwnDecPtr_rbn(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    m = rwnDecPtr_encode(args[0])
    n = rbn_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    return res

# OP rbn, [rwm + data16]
def rbn_rwnPlusOffsetPtr(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    n = rbn_encode(args[0])
    m, offset = rwnPlusOffsetPtr_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    res.extend(offset)
    return res

# OP [rwm + data16], rbn
def rwnPlusOffsetPtr_rbn(opcode: int, *args, **kwargs):
    assert correct_structure(args, 2, [Tree, Tree])
    m, offset = rwnPlusOffsetPtr_encode(args[0])
    n = rbn_encode(args[1])
    
    res = bytearray(2)
    res[0] = opcode
    res[1] = (n << 4) | m
    res.extend(offset)
    return res

