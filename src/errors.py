"""
Set of tools for info/warning/error messages raised during runtime
"""
from enum import Enum

class Origin(Enum):
    BUILDSYSTEM = 0
    PREPROCESSOR = 1
    ASSEMBLER = 2
    LINKER = 3

class MessageType(Enum):
    INFO = 0
    WARNING = 1
    ERROR = 2

# Name and Color for every origin
# ANSI codes as defined in https://i.sstatic.net/KTSQa.png
originMetas = {
        Origin.BUILDSYSTEM: ("Buildsystem", '128'),
        Origin.PREPROCESSOR: ("Preprocessor", '136'),
        Origin.ASSEMBLER: ("Assembler", '76'),
        Origin.LINKER: ("Linker", '32')
    }

typeMetas = {
        MessageType.INFO: ("Info", '21'),
        MessageType.WARNING: ("Warning", '214'),
        MessageType.ERROR: ("Error", '196')
    }

dyeByOrigin = lambda string, origin:  \
    "\033[38;5;" + originMetas[origin][1] + "m" + \
    f"[{string}]" \
    + "\033[0m"

dyeByType = lambda string, msgType: \
    "\033[38;5;" + typeMetas[msgType][1] + "m" + \
    f"[{string}]" \
    + "\033[0m"

def logError(origin: Origin, msgType: MessageType, msg: str, stop_execution=False):
    print(dyeByOrigin(originMetas[origin][0], origin) + " " + 
            dyeByType(typeMetas[msgType][0], msgType) + " " +
            msg)
    if stop_execution:
        exit(origin.value + 1);

