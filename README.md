# acigg
Primitive assembler for the C166/C167 family of processors (specifically targeting the hardware of the Siemens S45 and ME45).
This software comes with absolutely no warranty.

## Usage
IMPORTANT: As of now, only single source single binary building actually works (no includes)

Using manual input/output files: `acigg.py -i <input.asm> -o <output.bin>`  
Using a projet description: `acigg.py -p <project.yml>`  

It is highly recommended to use a project file for easier build management. Project files are YAML files that follow the following structire:
```yaml
%YAML 1.2
---
title: "My awesome project"
globalRegset: "s45"
binaries:
  - name: "My first binary target"
    destination: "out/main.bin"
    sources:
      - "src/main.asm"
      - "src/data.asm"
    includes:
      - "src/someCoroutine.asm"
  - name: "My second binary target"
  ...
```

## Roadmap
- Fully working preprocessor with inline directives
- Includes and file trees / multiple sources in general
- Documentation on undocumented registers

## Contributing
Feel free to submit issues and pull requests.

## License
GPLv3
