;; This is the bootFromSerial routine as present in ROM:03C2 on the S45

@regset s45 

@define CODEDEST 0xFA00

@org 0x0103C2
bootFromSerial:
  mov ~S0TBUF, 0xA0

  .prepare_code_rx:
    callr S0_wait_for_rx
    movbz r4, [&S0RBUF]
    mov r0, CODEDEST
    add r4, r0
    movb rl5, 0x0 

  .receive_code_byte:
    callr S0_wait_for_rx
    movb [r0], [&S0RBUF]
    xorb rl5, [r0+]
    cmp r0, r3
    jmpr cc_ne, .receive_code_byte

  callr S0_wait_for_rx
  cmpb rl5, [&S0RBUF]
  jmpr cc_eq, .code_checksum_valid
  mov ~S0TBUF, 0x5A
  jmpr cc_uc, .prepare_code_rx

  .code_checksum_valid:
    mov ~S0TBUF, 0xA5 
    bclr ~S0TIC.7
    .await_end:
      jnb ~S0TIC.7, .await_end ; Loop until transmission end

    jmps 0x00, CODEDEST ; Jump to rxed code 

;@org 0x01045C
S0_wait_for_rx:
  jnb ~S0RIC.7, S0_wait_for_rx
  bclr ~S0RIC.7
  ret

